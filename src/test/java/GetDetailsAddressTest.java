import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


public class GetDetailsAddressTest {


    @Test
    public void getDetailsAddressWithPostCode90210(){

        //Arrange

        //Act
        given().
                log().all().
        when().
                get("http://zippopotam.us/us/90210").
        then().
                log().all().
        //Assert
                statusCode(200).
                body("'post code'", equalTo("90210")).
                body("country", equalTo("United States")).
                body("'country abbreviation'", equalTo("US")).
                //basic way to validate
                body("places[0].'place name'", equalTo("Beverly Hills")).
                body("places[0].longitude", equalTo("-118.4065")).
                body("places[0].state", equalTo("California")).
                body("places[0].'state abbreviation'", equalTo("CA")).
                body("places[0].latitude", equalTo("34.0901"));
    }
}
